
<!-- README.md is generated from README.Rmd. Please edit that file -->

# paraols

<!-- badges: start -->

<!-- badges: end -->

The goal of paraols is to estimates a linear models using parallel
computing

## Installation

You can install the released version of paraols from
[CRAN](https://CRAN.R-project.org) with:

``` r
install.packages("paraols")
```

You can install the package from GitLab.

``` r
devtools::install_git("https://gitlab.com/nathanielUCP/paraols")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(paraols)
data(X)
paraOLS(X[,-c(1)], X[,1])
```
