context("Test output for paraOLS")

test_that("output of paraOLS match that of lm", {
  expect_equal(as.vector(paraOLS(X[,-c(1)], X[,1])),
               as.vector(lm(y~X2+X3+X4+X5, data=X)$coefficient)
  )
})
